# killztekiller

An [Xposed Framework](http://repo.xposed.info/module/de.robv.android.xposed.installer) module that disables the zte power manager app killer that kills apps when locking the screen.  This should only be used on an axon 7 running b27.

This will only help in the case where the logcat indicates "killing ... whatever ... excessive high power cost" or something like that

Get it from the [repository](http://repo.xposed.info/module/com.azhao.killztekiller).
