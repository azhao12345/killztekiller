package com.azhao.killztekiller;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import android.util.Log;

public class KillZTEKiller implements IXposedHookLoadPackage {

    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if(!loadPackageParam.packageName.equals("android"))
            return;

        try {
            XposedHelpers.findAndHookMethod("com.android.server.am.ActivityManagerService", loadPackageParam.classLoader, "checkExcessivePowerUsageLocked", "boolean", XC_MethodReplacement.DO_NOTHING);
            /*
            XposedHelpers.findAndHookMethod("com.android.server.am.ActivityManagerService", loadPackageParam.classLoader, "checkExcessivePowerUsageLocked", "boolean", new XC_MethodReplacement() {
                @Override
                protected Object replaceHookedMethod(MethodHookParam methodHookParam) throws Throwable {
                    Log.w("wow123",loadPackageParam.packageName);
                    return null;
                }
            });
            */
        } catch (Throwable e) {
            XposedBridge.log(e.toString());
        }
    }
}
